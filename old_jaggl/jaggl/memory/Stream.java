/* NativeStream - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jaggl.memory;

public final class Stream {
	private byte[] aByteArray1153;
	private NativeBuffer aNativeBuffer1154;
	private int anInt1155;
	private int anInt1156;

	public final int method560() {
		try {
			return anInt1156 + anInt1155;
		} catch (RuntimeException runtimeexception) {
			throw runtimeexception;
		}
	}

	public final void method561() {
		do {
			try {
				if ((anInt1156 ^ 0xffffffff) >= -1)
					break;
				aNativeBuffer1154.method697(aByteArray1153, anInt1155,
						anInt1156);
				anInt1155 += anInt1156;
				anInt1156 = 0;
			} catch (RuntimeException runtimeexception) {
				throw runtimeexception;
			}
			break;
		} while (false);
	}

	public static final native int floatToRawIntBits(float f);

	private static final native byte getLSB(int i);

	public static final boolean method562() {
		try {
			if (getLSB(-65536) != -1)
				return false;
			return true;
		} catch (RuntimeException runtimeexception) {
			runtimeexception.printStackTrace();
			throw runtimeexception;
		}
	}

	public final void method563(float f) {
		try {
			if (aByteArray1153.length <= 3 + anInt1156)
				method561();
			int i = floatToRawIntBits(f);
			aByteArray1153[anInt1156++] = (byte) (i >> 24);
			aByteArray1153[anInt1156++] = (byte) (i >> 16);
			aByteArray1153[anInt1156++] = (byte) (i >> 8);
			aByteArray1153[anInt1156++] = (byte) i;
		} catch (RuntimeException runtimeexception) {
			throw runtimeexception;
		}
	}

	public final void method564(int i) {
		try {
			method561();
			anInt1155 = i;
		} catch (RuntimeException runtimeexception) {
			throw runtimeexception;
		}
	}

	public final void method565(float f) {
		try {
			if (aByteArray1153.length <= anInt1156 + 3)
				method561();
			int i = floatToRawIntBits(f);
			aByteArray1153[anInt1156++] = (byte) i;
			aByteArray1153[anInt1156++] = (byte) (i >> 8);
			aByteArray1153[anInt1156++] = (byte) (i >> 16);
			aByteArray1153[anInt1156++] = (byte) (i >> 24);
		} catch (RuntimeException runtimeexception) {
			throw runtimeexception;
		}
	}

	public final void method566(int i) {
		try {
			if (aByteArray1153.length <= anInt1156)
				method561();
			aByteArray1153[anInt1156++] = (byte) i;
		} catch (RuntimeException runtimeexception) {
			throw runtimeexception;
		}
	}

	private Stream(NativeBuffer nativebuffer, int i) {
		try {
			aByteArray1153 = new byte[i];
			aNativeBuffer1154 = nativebuffer;
		} catch (RuntimeException runtimeexception) {
			throw runtimeexception;
		}
	}

	public Stream(NativeBuffer nativebuffer) {
		this(nativebuffer,
				nativebuffer.anInt1457 < 4096 ? nativebuffer.anInt1457 : 4096);
	}
}
