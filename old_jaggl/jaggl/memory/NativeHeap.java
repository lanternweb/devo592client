/* NativeHeap - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jaggl.memory;

public class NativeHeap
{
    long peer;
    private int anInt926;
    private boolean aBool927;
    
    private final native void deallocateHeap();
    
    public final NativeBuffer method454(int i) {
	try {
	    if (!aBool927)
		throw new IllegalStateException();
	    return new NativeBuffer(this, allocateBuffer(i), i);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	}
    }
    
    final synchronized native int allocateBuffer(int i);
    
    final synchronized native void put(int i, byte[] is, int i_0_, int i_1_);
    
    protected final synchronized void finalize() throws Throwable {
	try {
	    super.finalize();
	    method456();
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	}
    }
    
    final synchronized native void get(int i, byte[] is, int i_2_, int i_3_);
    
    final synchronized boolean method455() {
	try {
	    return aBool927;
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	}
    }
    
    final synchronized native long getBufferAddress(int i);
    
    public final synchronized void method456() {
	try {
	    if (aBool927)
		deallocateHeap();
	    aBool927 = false;
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	}
    }
    
    final synchronized native void deallocateBuffer(int i);
    
    private final native void allocateHeap(int i);
    
    public NativeHeap(int i) {
	try {
	    anInt926 = i;
	    allocateHeap(anInt926);
	    aBool927 = true;
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	}
    }
}
