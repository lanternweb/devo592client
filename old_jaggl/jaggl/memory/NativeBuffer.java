/* NativeBuffer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jaggl.memory;

public final class NativeBuffer
{
    private boolean aBool1454 = true;
    private int anInt1455;
    private NativeHeap aNativeHeap1456;
    public int anInt1457;
    
    protected final synchronized void finalize() throws Throwable {
	try {
	    super.finalize();
	    method699();
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	}
    }
    
    public final synchronized void method697(byte[] is, int i, int i_0_) {
	try {
	    if ((i ^ 0xffffffff) > -1
		| (!method698() | is == null
		   | (is.length ^ 0xffffffff) > (i_0_ ^ 0xffffffff))
		| (anInt1457 ^ 0xffffffff) > (i - -i_0_ ^ 0xffffffff))
		throw new RuntimeException();
	    aNativeHeap1456.put(anInt1455, is, i, i_0_);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	}
    }
    
    private final synchronized boolean method698() {
	try {
	    if (!aNativeHeap1456.method455() || !aBool1454)
		return false;
	    return true;
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	}
    }
    
    private final synchronized void method699() {
	try {
	    if (method698())
		aNativeHeap1456.deallocateBuffer(anInt1455);
	    aBool1454 = false;
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	}
    }
    
    NativeBuffer(NativeHeap nativeheap, int i, int i_1_) {
	try {
	    aNativeHeap1456 = nativeheap;
	    anInt1455 = i;
	    anInt1457 = i_1_;
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	}
    }
    
    public final long method700() {
	try {
	    return aNativeHeap1456.getBufferAddress(anInt1455);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	}
    }
}
