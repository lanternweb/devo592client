/* OpenGL - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jaggl;
import java.awt.Canvas;
import java.util.Hashtable;

public class OpenGL
{
    long peer;
    private Hashtable aHashtable674;
    private Thread aThread675;
    private static Hashtable aHashtable676 = new Hashtable();
    
    public static native void glTexCoord2i(int i, int i_0_);
    
    public static native void glBindFramebufferEXT(int i, int i_1_);
    
    public static native void glEnd();
    
    public static native void glBufferDataARBub(int i, int i_2_, byte[] is,
						int i_3_, int i_4_);
    
    public native long createPbuffer(int i, int i_5_);
    
    public static native void glGetObjectParameterivARB(long l, int i,
							int[] is, int i_6_);
    
    public static native void glLineWidth(float f);
    
    public static native void glDisable(int i);
    
    public static native void glReadPixelsi(int i, int i_7_, int i_8_,
					    int i_9_, int i_10_, int i_11_,
					    int[] is, int i_12_);
    
    public native long prepareSurface(Canvas canvas);
    
    public static native void glGenBuffersARB(int i, int[] is, int i_13_);
    
    public static native void glFogi(int i, int i_14_);
    
    public static native void glDepthMask(boolean bool);
    
    public static native void glRotatef(float f, float f_15_, float f_16_,
					float f_17_);
    
    public static native void glLoadMatrixf(float[] fs, int i);
    
    public static native void glGetProgramivARB(int i, int i_18_, int[] is,
						int i_19_);
    
    public static native void glLoadIdentity();
    
    public static native void glPixelStorei(int i, int i_20_);
    
    public static native void glTexCoordPointer(int i, int i_21_, int i_22_,
						long l);
    
    public static native String glGetString(int i);
    
    public static native void glBufferSubDataARB(int i, int i_23_, int i_24_,
						 byte[] is, int i_25_);
    
    public static native void glTexEnvi(int i, int i_26_, int i_27_);
    
    public static native void glScalef(float f, float f_28_, float f_29_);
    
    public static native void glTexGenfv(int i, int i_30_, float[] fs,
					 int i_31_);
    
    public static native void glActiveTexture(int i);
    
    public static native void glCopyPixels(int i, int i_32_, int i_33_,
					   int i_34_, int i_35_);
    
    public static OpenGL getCurrent() {
	try {
	    return (OpenGL) aHashtable676.get(Thread.currentThread());
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	}
    }
    
    public static native void glVertexPointer(int i, int i_36_, int i_37_,
					      long l);
    
    public static native void glBindProgramARB(int i, int i_38_);
    
    public static native void glCopyTexSubImage3D
	(int i, int i_39_, int i_40_, int i_41_, int i_42_, int i_43_,
	 int i_44_, int i_45_, int i_46_);
    
    public static native void glDrawElements(int i, int i_47_, int i_48_,
					     long l);
    
    public static native void glTexImage2Dub
	(int i, int i_49_, int i_50_, int i_51_, int i_52_, int i_53_,
	 int i_54_, int i_55_, byte[] is, int i_56_);
    
    public static native void glAttachObjectARB(long l, long l_57_);
    
    public static native void glReadBuffer(int i);
    
    public static native void glTexImage2Df
	(int i, int i_58_, int i_59_, int i_60_, int i_61_, int i_62_,
	 int i_63_, int i_64_, float[] fs, int i_65_);
    
    public static native void glDisableClientState(int i);
    
    public static native void glColorPointer(int i, int i_66_, int i_67_,
					     long l);
    
    public static native void glTexImage3Dub
	(int i, int i_68_, int i_69_, int i_70_, int i_71_, int i_72_,
	 int i_73_, int i_74_, int i_75_, byte[] is, int i_76_);
    
    public static native void glTexSubImage2Dub
	(int i, int i_77_, int i_78_, int i_79_, int i_80_, int i_81_,
	 int i_82_, int i_83_, byte[] is, int i_84_);
    
    public static native int glGenProgramARB();
    
    public static native void glBindTexture(int i, int i_85_);
    
    public static native void glClear(int i);
    
    public native boolean setSurface(long l);
    
    public static native void glDeleteLists(int i, int i_86_);
    
    public static native void glFogf(int i, float f);
    
    public static native void glPolygonMode(int i, int i_87_);
    
    public static native void glScissor(int i, int i_88_, int i_89_,
					int i_90_);
    
    public static native void glProgramLocalParameter4fvARB
	(int i, int i_91_, float[] fs, int i_92_);
    
    public native void setPbuffer(long l);
    
    public static native void glBindRenderbufferEXT(int i, int i_93_);
    
    public static native void glDetachObjectARB(long l, long l_94_);
    
    public static native void glCompileShaderARB(long l);
    
    public static native void glFramebufferTexture2DEXT
	(int i, int i_95_, int i_96_, int i_97_, int i_98_);
    
    public static native void glBlendFunc(int i, int i_99_);
    
    public static native void glPointSize(float f);
    
    public static native void glTexImage2Di
	(int i, int i_100_, int i_101_, int i_102_, int i_103_, int i_104_,
	 int i_105_, int i_106_, int[] is, int i_107_);
    
    public static native void glTexImage1Dub
	(int i, int i_108_, int i_109_, int i_110_, int i_111_, int i_112_,
	 int i_113_, byte[] is, int i_114_);
    
    public static native void glDeleteObjectARB(long l);
    
    public static native void glGetFloatv(int i, float[] fs, int i_115_);
    
    public static native void glTexCoord3f(float f, float f_116_,
					   float f_117_);
    
    public boolean isExtensionAvailable(String string) {
	try {
	    if (aHashtable674 == null) {
		aHashtable674 = new Hashtable();
		String string_118_ = glGetString(7939);
		int i = 0;
		for (;;) {
		    int i_119_ = string_118_.indexOf(' ', i);
		    if (i_119_ == -1)
			break;
		    String string_120_
			= string_118_.substring(i, i_119_).trim();
		    if ((string_120_.length() ^ 0xffffffff) != -1)
			aHashtable674.put(string_120_, string_120_);
		    i = 1 + i_119_;
		}
		String string_121_ = string_118_.substring(i).trim();
		if (string_121_.length() != 0)
		    aHashtable674.put(string_121_, string_121_);
	    }
	    return aHashtable674.containsKey(string);
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	}
    }
    
    public static native void glFinish();
    
    public static native void glBegin(int i);
    
    public static native void glColor4f(float f, float f_122_, float f_123_,
					float f_124_);
    
    public static native void glLightModelfv(int i, float[] fs, int i_125_);
    
    public static native void glRenderbufferStorageEXT(int i, int i_126_,
						       int i_127_, int i_128_);
    
    public static native void glTexGeni(int i, int i_129_, int i_130_);
    
    public static native void glLightf(int i, int i_131_, float f);
    
    public static native void glShadeModel(int i);
    
    public static native void glUniform2fARB(int i, float f, float f_132_);
    
    public static native void glMaterialfv(int i, int i_133_, float[] fs,
					   int i_134_);
    
    public static native void glDrawBuffersARB(int i, int[] is, int i_135_);
    
    public static native void glFramebufferTexture3DEXT
	(int i, int i_136_, int i_137_, int i_138_, int i_139_, int i_140_);
    
    public static native void glUniform1iARB(int i, int i_141_);
    
    public static native void glTexParameteri(int i, int i_142_, int i_143_);
    
    public static native void glProgramLocalParameter4fARB
	(int i, int i_144_, float f, float f_145_, float f_146_, float f_147_);
    
    public static native void glGetInfoLogARB
	(long l, int i, int[] is, int i_148_, byte[] is_149_, int i_150_);
    
    public static native void glClientActiveTexture(int i);
    
    public static native void glUniform3fARB(int i, float f, float f_151_,
					     float f_152_);
    
    public static native void glColor3ub(byte i, byte i_153_, byte i_154_);
    
    public static native void glDeleteTextures(int i, int[] is, int i_155_);
    
    public native void releaseSurface(Canvas canvas, long l);
    
    public static native void glDeleteBuffersARB(int i, int[] is, int i_156_);
    
    public static native void glDeleteRenderbuffersEXT(int i, int[] is,
						       int i_157_);
    
    public static native long glCreateProgramObjectARB();
    
    public static native void glColor3f(float f, float f_158_, float f_159_);
    
    public static native void glPushAttrib(int i);
    
    public static native void glLightfv(int i, int i_160_, float[] fs,
					int i_161_);
    
    public static native void glTexSubImage2Df
	(int i, int i_162_, int i_163_, int i_164_, int i_165_, int i_166_,
	 int i_167_, int i_168_, float[] fs, int i_169_);
    
    public static native void glFrustum(double d, double d_170_, double d_171_,
					double d_172_, double d_173_,
					double d_174_);
    
    public static native void glUniform1fARB(int i, float f);
    
    public static native void glTexEnvfv(int i, int i_175_, float[] fs,
					 int i_176_);
    
    private native boolean attachPeer();
    
    public static native void glReadPixelsub
	(int i, int i_177_, int i_178_, int i_179_, int i_180_, int i_181_,
	 byte[] is, int i_182_);
    
    public static native void glBlitFramebufferEXT
	(int i, int i_183_, int i_184_, int i_185_, int i_186_, int i_187_,
	 int i_188_, int i_189_, int i_190_, int i_191_);
    
    public static native void glNormalPointer(int i, int i_192_, long l);
    
    public static native void glColor4ub(byte i, byte i_193_, byte i_194_,
					 byte i_195_);
    
    public static native void glGetIntegerv(int i, int[] is, int i_196_);
    
    public static native void glGenRenderbuffersEXT(int i, int[] is,
						    int i_197_);
    
    public native boolean arePbuffersAvailable();
    
    public static native void glCullFace(int i);
    
    public static native void glOrtho(double d, double d_198_, double d_199_,
				      double d_200_, double d_201_,
				      double d_202_);
    
    public static native void glCopyTexSubImage2D
	(int i, int i_203_, int i_204_, int i_205_, int i_206_, int i_207_,
	 int i_208_, int i_209_);
    
    public static native void glNewList(int i, int i_210_);
    
    public static native int glGetUniformLocationARB(long l, String string);
    
    public synchronized boolean detach() {
	try {
	    if (aThread675 != Thread.currentThread())
		return false;
	    detachPeer();
	    aHashtable676.remove(aThread675);
	    aThread675 = null;
	    return true;
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	}
    }
    
    public static native void glDeleteFramebuffersEXT(int i, int[] is,
						      int i_211_);
    
    public static native long glCreateShaderObjectARB(int i);
    
    public static native void glVertex2f(float f, float f_212_);
    
    public static native void glAlphaFunc(int i, float f);
    
    public static native int glGenLists(int i);
    
    public static native void glUniform4fARB(int i, float f, float f_213_,
					     float f_214_, float f_215_);
    
    public static native void glEndList();
    
    public static native void glGenerateMipmapEXT(int i);
    
    public static native void glLinkProgramARB(long l);
    
    public static native void glRenderbufferStorageMultisampleEXT
	(int i, int i_216_, int i_217_, int i_218_, int i_219_);
    
    public static native void glClearColor(float f, float f_220_, float f_221_,
					   float f_222_);
    
    public static native void glVertex3f(float f, float f_223_, float f_224_);
    
    public static native void glFogfv(int i, float[] fs, int i_225_);
    
    public static native void glGenFramebuffersEXT(int i, int[] is,
						   int i_226_);
    
    private native void detachPeer();
    
    public static native void glDeleteProgramARB(int i);
    
    public static native void glVertex2i(int i, int i_227_);
    
    public static native void glShaderSourceARB(long l, String string);
    
    public static native void glUseProgramObjectARB(long l);
    
    public static native void glHint(int i, int i_228_);
    
    public static native void glColorMaterial(int i, int i_229_);
    
    public static native void glCallList(int i);
    
    public static native void glDrawArrays(int i, int i_230_, int i_231_);
    
    public static native void glCopyTexImage2D
	(int i, int i_232_, int i_233_, int i_234_, int i_235_, int i_236_,
	 int i_237_, int i_238_);
    
    public static native void glPixelZoom(float f, float f_239_);
    
    public static native void glPopAttrib();
    
    public static native void glMultiTexCoord2f(int i, float f, float f_240_);
    
    public native void releasePbuffer(long l);
    
    public static native void glPixelTransferf(int i, float f);
    
    public static native void glTexCoord2f(float f, float f_241_);
    
    public native void setSwapInterval(int i);
    
    public static native void glTexParameterf(int i, int i_242_, float f);
    
    public native void swapBuffers();
    
    public static native void glRasterPos2i(int i, int i_243_);
    
    public static native void glMultiTexCoord2i(int i, int i_244_, int i_245_);
    
    public static native void glFramebufferRenderbufferEXT
	(int i, int i_246_, int i_247_, int i_248_);
    
    public static native void glEnableClientState(int i);
    
    public static native void glEnable(int i);
    
    public static native void glClearDepth(float f);
    
    public static native void glTranslatef(float f, float f_249_,
					   float f_250_);
    
    public static native void glBufferDataARBa(int i, int i_251_, long l,
					       int i_252_);
    
    public static native void glDrawPixelsi(int i, int i_253_, int i_254_,
					    int i_255_, int[] is, int i_256_);
    
    public static native void glNormal3f(float f, float f_257_, float f_258_);
    
    public static native void glTexSubImage2Di
	(int i, int i_259_, int i_260_, int i_261_, int i_262_, int i_263_,
	 int i_264_, int i_265_, int[] is, int i_266_);
    
    public static native int glGetError();
    
    public static native void glBindBufferARB(int i, int i_267_);
    
    public synchronized boolean attach() {
	try {
	    Thread thread = Thread.currentThread();
	    if (attachPeer()) {
		OpenGL opengl_268_ = (OpenGL) aHashtable676.put(thread, this);
		if (opengl_268_ != null)
		    opengl_268_.aThread675 = null;
		aThread675 = thread;
		return true;
	    }
	    return false;
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	}
    }
    
    public static native void glMatrixMode(int i);
    
    public native long init(Canvas canvas, int i, int i_269_, int i_270_,
			    int i_271_, int i_272_, int i_273_);
    
    public native void surfaceResized(long l);
    
    public static native void glDrawBuffer(int i);
    
    public static native int glCheckFramebufferStatusEXT(int i);
    
    public static native void glColorMask(boolean bool, boolean bool_274_,
					  boolean bool_275_,
					  boolean bool_276_);
    
    public static native void glPushMatrix();
    
    public static native void glDrawPixelsub
	(int i, int i_277_, int i_278_, int i_279_, byte[] is, int i_280_);
    
    public static native void glMultMatrixf(float[] fs, int i);
    
    public static native void glProgramStringARB(int i, int i_281_,
						 String string);
    
    public static native void glPopMatrix();
    
    public static native void glTexEnvf(int i, int i_282_, float f);
    
    public static native void glGenTextures(int i, int[] is, int i_283_);
    
    public static native void glViewport(int i, int i_284_, int i_285_,
					 int i_286_);
    
    public native void release();
    
    public static native void glFlush();
    
    public static native void glDepthFunc(int i);
}
